import * as ActionTypes from '../constants/ActionTypes';
import { BaseUrl } from '../constants/api';
import {Alert, } from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
export function loginRes(data) {
    return {
        type: ActionTypes.LOGIN,
        data
    }
};
export function loginAPI(data) {
 console.log("loginapi",data)
 return (dispatch) => {
 fetch(BaseUrl + `/login`, {
method: 'POST',
headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         },
body: JSON.stringify(data)
})
.then((res) => res.json())
.then(res => {
console.log("login res",res)

if (res.success === true) {
    AsyncStorage.setItem('user_id',res.userData._id);
    AsyncStorage.setItem('email',res.userData.email);
    AsyncStorage.setItem('firstName',res.userData.firstName);
    AsyncStorage.setItem('lastName',res.userData.lastName);
    AsyncStorage.setItem('pin',res.userData.pin);
    AsyncStorage.setItem('token',res.token);
    AsyncStorage.setItem('role',res.userData.role.toString());

    dispatch(loginRes(res));
} else  {
    // Alert.alert(res.message)
    dispatch(loginRes(res))
}
})
.catch((e) => {
console.warn(e);
});
}
};

