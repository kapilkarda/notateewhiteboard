// import React from 'react';
// import {
//   View,
//   Image,
//   Text,
//   ScrollView,
//   KeyboardAvoidingView,
//   TouchableOpacity,
// } from 'react-native';
// import {Actions} from 'react-native-router-flux';
// // import PropTypes from 'prop-types';
// import {connect} from 'react-redux';
// import styles from './styles';
// import fonts from '../../theme/fonts';
// import {w, h} from '../../utils/Dimensions';
// import {drawermenuAPI, resetDrawerRes} from '../../actions/DrawerMenu';
// import AsyncStorage from '@react-native-community/async-storage';
// // import {closeDatabase} from '../../database';

// class DrawerBar extends React.Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       isLoading: false,
//       email: '',
//       firstName: '',
//       lastName: '',
//       pin: '',
//       logoutstatus: '',
//       token: '',
//     };
//   }
//   toggleDrawer = () => {
//     Actions.drawerToggle();
//   };
//   ViewReport = () => {
//     Actions.ViewReport();
//   };
//   settingsScreen = () => {
//     Actions.Settings();
//   };
//   componentWillMount = async () => {
//     var email = await AsyncStorage.getItem('email');
//     var token = await AsyncStorage.getItem('token');
//     var pin = await AsyncStorage.getItem('pin');
//     this.setState(
//       {
//         email: email,
//         token: token,
//         pin: pin,
//       }
//     );
//   };

//   signOut = async () => {

//     AsyncStorage.removeItem('firstName');
//     AsyncStorage.removeItem('lastName');
    
    
//     let payload = {token: this.state.token, email: this.state.email};
//     const resetdata = {};
//     this.props.resetForm({resetdata})
//     this.props.submitForm(payload);
//     Actions.drawerClose();
//     this.setState({
//       logoutstatus: 'logout',
//     });
//     AsyncStorage.clear();
//   };

//   componentWillReceiveProps = async (nextProps) => {
//     const firstName = await AsyncStorage.getItem('firstName');
//     const lastName = await AsyncStorage.getItem('lastName');
//      var email = await AsyncStorage.getItem('email');
//     this.setState({
//       email: email,
//       firstName: firstName,
//       lastName: lastName,
//     });
//     if (nextProps.drawermenu) {
//       if (
//         nextProps.drawermenu.statusCode === 200 &&
//         this.state.logoutstatus == 'logout'
//       ) {
//         Actions.Login();
//         // closeDatabase();
//         this.setState({
//           logoutstatus : ""
//         })
//       }
//     }
//   };

//   render() {
//     return (
//       <KeyboardAvoidingView style={{flex: 1}} behavior="padding" enabled>
//         <View style={styles.mainContainer}>
//           <View style={styles.user}>
//             <View style={styles.empImageView}>
//               <Image
//                 source={require('../../assets/image/AliceCooper.jpg')}
//                 style={styles.empImage}
//               />
//             </View>

//             <View style={styles.userDetail}>
//               <Text style={styles.userName}>
//                 {this.state.firstName} {this.state.lastName}
//               </Text>
//               <Text style={styles.userID}> Employee ID</Text>
//             </View>
//           </View>
//           <View style={styles.drawerList}>
//             <TouchableOpacity onPress={Actions.drawerToggle}>
//               <View style={styles.drawerView}>
//                 <Image
//                   style={styles.inputIcon}
//                   source={require('../../assets/icon/sites.png')}
//                   resizeMode="contain"
//                 />
//                 <Text style={styles.drawerText}>Home</Text>
//               </View>
//             </TouchableOpacity>
//             <TouchableOpacity onPress={() => this.ViewReport()}>
//               <View style={styles.drawerView}>
//                 <Image
//                   style={styles.inputIcon}
//                   source={require('../../assets/icon/eye.png')}
//                   resizeMode="contain"
//                 />
//                 <Text style={styles.drawerText}>View Report</Text>
//               </View>
//             </TouchableOpacity>
//             <TouchableOpacity>
//               <View style={styles.drawerView}>
//                 <Image
//                   style={styles.inputIcon}
//                   source={require('../../assets/icon/syncData.png')}
//                   resizeMode="contain"
//                 />
//                 <View>
//                   <Text style={styles.drawerText}>Sync Data</Text>
//                   <Text
//                     style={{
//                       color: '#000',
//                       // fontFamily: fonts.lightText,
//                       fontSize: h(2),
//                       width: w(55),
//                     }}>
//                     Last sync time: 04:00 pm
//                   </Text>
//                 </View>
//               </View>
//             </TouchableOpacity>
//             <TouchableOpacity>
//               <View style={styles.drawerView}>
//                 <Image
//                   style={styles.inputIcon}
//                   source={require('../../assets/icon/settings.png')}
//                   resizeMode="contain"
//                 />
//                 <Text style={styles.drawerText}>Settings</Text>
//               </View>
//             </TouchableOpacity>
//             <TouchableOpacity onPress={() => this.signOut()}>
//               <View style={styles.drawerView}>
//                 <Image
//                   style={styles.inputIcon}
//                   source={require('../../assets/icon/logout.png')}
//                   resizeMode="contain"
//                 />
//                 <Text style={styles.drawerText}>Sign Out</Text>
//               </View>
//             </TouchableOpacity>
//           </View>
//         </View>
//       </KeyboardAvoidingView>
//     );
//   }
// }

// DrawerBar.propTypes = {};

// const mapStateToProps = (state) => {
//   return {
//     // login: state.login,
//     drawermenu: state.drawermenu,
//   };
// };

// const mapDispatchToProps = (dispatch) => {
//   return {
//     submitForm: (data) => dispatch(drawermenuAPI(data)),
//     resetForm: (data) => dispatch(resetDrawerRes(data)),
//   };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(DrawerBar);
