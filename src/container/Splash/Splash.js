import React from 'react';
import { View, Animated, StatusBar, Image } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import styles from './styles';
import splashImg from '../../assets/Images/SplashScreen.jpg';
import AsyncStorage from '@react-native-community/async-storage';

class Splash extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fadeValue: new Animated.Value(0),
      user_id:""
    }
  }

  componentDidMount = async () => {
    try {
      setTimeout(async () => {
          Actions.VideoCallExample();
      }, 1500);
    } catch (error) {
      console.log('error' + error);
    }
  }
 
  render() {
    return (
      <View style={styles.container}>
        <StatusBar />
        <Image resizeMode='cover' source={splashImg} style={styles.splashImg} />
      </View>
    );
  }
}

export default connect(null, null)(Splash);