import { StyleSheet } from 'react-native';
import { h, w } from '../../utils/Dimensions';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  splashImg: {
    height: '100%',
    width: '100%',
  },
});