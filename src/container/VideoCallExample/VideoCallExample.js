import React from 'react';
import {
  StyleSheet,
  View,
  Platform,
} from 'react-native';
import { WebView } from 'react-native-webview';

const defaultOptions = {
  messageStyle: 'none',
  extensions: ['tex2jax.js'],
  jax: ['input/TeX', 'output/HTML-CSS'],
  tex2jax: {
    inlineMath: [['$$', '$$'], ['\\(', '\\)']],
    displayMath: [['$$', '$$'], ['\\[', '\\]']],
    processEscapes: true,
  },
  TeX: {
    extensions: ['AMSmath.js', 'AMSsymbols.js', 'noErrors.js', 'noUndefined.js']
  }
};

const paperContent = {
  content: `<p>  If the vector from your tent to Joe's is $$\\vec{A}$$ and from your tent to Karl's is $$\\vec{B}$$ , then the vector from Joe's tent to Karl's is $$\\vec{B}-\\vec{A}.$$
    </p>
    <p> Take your tent's position as the origin. Let $$+x$$ be east and $$+y$$ be north.
    </p>
    <p> The position vector for Joe's tent is
    </p><p>$$([21.0\\ \\mathrm{m}]\\cos\\ 23\\circ) \\hat{i}-([2\\mathrm{l}.0\\ \\mathrm{m}]\\sin 23^{\\mathrm{o}})\\hat{j}=$$($$19.33 \\mathrm{m})\\hat{i}-($$8.205 $$\\mathrm{m})\\hat{j}.$$
    </p><p>The position vector for Karl's tent is $$([32.0\\ \\mathrm{m}]\\cos\\ 37) \\hat{i}+ ([32.0\\ \\mathrm{m}]\\sin 37^{\\mathrm{o}})\\hat{j}=$$($$25.56m)\\hat{i}+ ($$19.26 $$\\mathrm{m})\\hat{j}.$$ The difference between the two positions is
    </p>
    <p>$$(19.33 m-25.56 m)\\hat{i}+(-8.205 m-19.25 \\mathrm{m})\\hat{j}=$$-$$(6.23 m)\\hat{i}-(27.46 \\mathrm{m})\\hat{j}$$. The magnitude of this vector is the distance between the two tents: $$D=\\sqrt{(-623\\mathrm{m})^{2}+(-2746\mathrm{m})^{2}}=28.2\\mathrm{m}$$
    </p>
    <p> If  both tents were due east of yours, the distance between them would be
    </p>
    <p>32.0 m-21.0 $$\\mathrm{m}=11.0\\mathrm{m}$$. If Joe's was due north of yours and Karl's was due south of yours, then the
    </p>
    <p>distance between them would be 32.0 $$\\mathrm{m}+21.0\\mathrm{m}=53.0\\mathrm{m}$$. The actual distance between them lies
    </p>
    <p>between these limiting values.
    </p>`
}

export default class VideoCallExample extends React.Component {
  state = {
    height: 1
  }

  handleMessage(message) {
    this.setState({
      height: Number(message.nativeEvent.data)
    });
  }


  wrapMathjax(content) {
    const options = JSON.stringify(
      Object.assign({}, defaultOptions, this.props.mathJaxOptions)
    );

    return `
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
			<script type="text/x-mathjax-config">
				MathJax.Hub.Config(${options});
				MathJax.Hub.Queue(function() {
					var height = document.documentElement.scrollHeight;
					window.postMessage(String(height));
					document.getElementById("formula").style.visibility = '';
				});
			</script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js"></script>
      <div id="formula" style="visibility: hidden;">
       ${paperContent.content}
			</div>
		`;
  }

  render() {
    const html = this.wrapMathjax(this.props.html);

    // Create new props without `props.html` field. Since it's deprecated.
    const props = Object.assign({}, this.props, { html: undefined });
    return (
      <View style={styles.container}>

        <View style={{ flex: 0.95 }}>
          <WebView
            scrollEnabled={true}
            onMessage={this.handleMessage.bind(this)}
            source={{ html }}
            {...props}
          />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 50
  },

  content: {
    flex: 1,
    flexDirection: 'row',
    padding: 20,
    justifyContent: 'center',
  },

  textInput: {
    borderColor: 'gray',
    borderWidth: 1,
    flex: 1,
    fontSize: 24
  },

  footer: {
    backgroundColor: Platform.OS === 'ios' ? '#eee' : '#fff',
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'center',
    paddingVertical: 20,
  },
});
