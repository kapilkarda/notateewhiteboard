import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    View,
    SafeAreaView,
    Alert,
    Image,
    Text,
    TouchableOpacity,
    Animated,
    ImageBackground
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from './styles';
import { w, h, height } from '../../utils/Dimensions';
import AsyncStorage from '@react-native-community/async-storage';
import RNSketchCanvas from '@terrylinla/react-native-sketch-canvas';
import ImagePicker from 'react-native-image-picker';

class WhiteBoard extends React.Component {
    constructor() {
        super();
        this.state = {
            active: false,
            colorArray: [],
            animation: false,
            ready: false,
            SlideInLeft: new Animated.Value(0),
            uiRender: false,
            filePath: {},
        };
    }

    chooseFile = () => {
        this.setState({
            animation: false
          });
        var options = {
          title: 'Select Image',
          customButtons: [
            { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
          ],
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            alert(response.customButton);
          } else {
            let source = response;
            this.setState({
              filePath: source,
            }, () => {
            console.log("filePathConsole", this.state.filePath);
            });
          }
        });
      };

    fileSave() {
        if(this.state.animation == false){
            this.setState({
                animation: true
            })
        } else {
            this.setState({
                animation: false
            })
        }
        if(!this.state.uiRender){
            this.setState({
                uiRender: true
            })
        } else {
            this.setState({
                uiRender: false
            })
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.canvasContainer}>
                    <View style={{ flex: 1, flexDirection: 'row',  }}>
                        <RNSketchCanvas
                            containerStyle={{ backgroundColor: 'transparent', flex: 1 }}
                            canvasStyle={{ backgroundColor: 'transparent', flex: 1, }}
                            defaultStrokeIndex={0}
                            defaultStrokeWidth={5}
                            undoComponent={
                                <View style={styles.functionButton}>
                                    <Image source={require('../../assets/Images/Vector.png')}
                                        style={{ width: h(2.5), resizeMode: 'contain', height: h(2.5), resizeMode: 'contain' }}
                                    />
                                </View>
                            }
                            eraseComponent={<View style={styles.undoButton}>
                                <Image source={require('../../assets/Images/Erase.png')}
                                    style={{ width: h(2.5), resizeMode: 'contain', height: h(2.5), resizeMode: 'contain' }}
                                />
                            </View>}
                            strokeComponent={color => (
                                <View style={[{ backgroundColor: color,}, styles.strokeColorButton]} />
                            )}
                            strokeSelectedComponent={(color, index, changed) => {
                                return (
                                    <View style={[{ backgroundColor: color, borderWidth: 2 }, styles.strokeColorButton]} />
                                )
                            }}
                            savePreference={() => {
                                return {
                                    folder: 'RNSketchCanvas',
                                    filename: String(Math.ceil(Math.random() * 100000000)),
                                    transparent: false,
                                    imageType: 'png'
                                }
                            }}
                            onSketchSaved={(success, filePath) => {
                                console.log('onSketchSaved', 'filePath: ' + filePath);
                             }}
                        />
                        {/* <View style={{ position: 'absolute', bottom: -5, right: -10, zIndex: 50, zIndex: 3, }}>
                                <ImageBackground source={require('../../assets/Images/Ellipse33.png')}
                                style={{
                                    width:100, 
                                    height:100, 
                                    resizeMode:'contain', 
                                    alignItems:'center', 
                                    justifyContent:'center',
                                    paddingTop:20,
                                    paddingLeft:10
                                }}
                                >
                                  <TouchableOpacity 
                                  onPress={() => this.fileSave()}
                                  style={{height:40, width:40,alignItems:'center', justifyContent:'center'}}>
                                      <Image source={require('../../assets/Images/home.png')} 
                                      style={{height:30, width:30, resizeMode:'contain'}}
                                      />
                                      </TouchableOpacity>  
                                </ImageBackground>
                                { this.state.animation == true ? (
                                    <View>
                                    <View style={{
                                        position:'absolute', 
                                        bottom:100, 
                                        height:50,
                                        width:50,
                                        right:10,
                                        }}>
                                            <TouchableOpacity style={{
                                                  height:40,
                                                  width:40,
                                                  borderRadius: 40/2,
                                                  backgroundColor:"rgba(86, 204, 242, 0.7)", 
                                                  alignItems:'center',
                                                  justifyContent:'center'
                                            }}>
                                                <Image source={require('../../assets/Images/Vector(4).png')}
                                                style={{height:20, width: 20, resizeMode:'contain'}}
                                                />
                                            </TouchableOpacity>
                                    </View>
                                     <View style={{
                                        position:'absolute', 
                                        bottom:65, 
                                        height:50,
                                        width:50,
                                        right:70
                                        }}>
                                             <TouchableOpacity style={{
                                                  height:40,
                                                  width:40,
                                                  borderRadius: 40/2,
                                                  backgroundColor:"rgba(86, 204, 242, 0.7)", 
                                                  alignItems:'center',
                                                  justifyContent:'center'
                                            }}>
                                                <Image source={require('../../assets/Images/exam.png')}
                                                style={{height:20, width: 20, resizeMode:'contain'}}
                                                />
                                            </TouchableOpacity>
                                    </View>
                                    <View style={{
                                        position:'absolute', 
                                        bottom:5, 
                                        height:50,
                                        width:50,
                                        right:100
                                        }}>
                                             <TouchableOpacity 
                                             onPress={this.chooseFile.bind(this)}
                                             style={{
                                                  height:40,
                                                  width:40,
                                                  borderRadius: 40/2,
                                                  backgroundColor:"rgba(86, 204, 242, 0.7)", 
                                                  alignItems:'center',
                                                  justifyContent:'center'
                                            }}>
                                                <Image source={require('../../assets/Images/Vector(2).png')}
                                                style={{height:20, width: 20, resizeMode:'contain'}}
                                                />
                                            </TouchableOpacity>
                                    </View>
                                    </View>
                                    
                                ):null
                                }
                        </View> */}
                      
                    </View>

                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(null, null)(WhiteBoard);