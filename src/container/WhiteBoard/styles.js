import { StyleSheet } from 'react-native';
import { w, h } from '../../utils/Dimensions';
import fonts from '../../theme/fonts'
export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#fff'
    },
    canvasContainer: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center', 
        backgroundColor: '#F5FCFF',
      },
      strokeColorButton: {
        marginHorizontal: 2.5, 
        marginVertical: 8, 
        width: 30, 
        height: 30, 
        borderRadius: 15,
      },
      strokeWidthButton: {
        marginHorizontal: 2.5, 
        marginVertical: 8, 
        width: 30, 
        height: 30, 
        borderRadius: 15,
        justifyContent: 'center', 
        alignItems: 'center', 
        backgroundColor: '#39579A'
      },
      functionButton: {
        marginHorizontal: 2.5, 
        marginVertical: 8, 
        height: 30, 
        width: 30,
        backgroundColor: '#C4C4C4', 
        justifyContent: 'center', 
        alignItems: 'center', 
        borderRadius: 30/2,
        marginRight:80
      },
      undoButton: {
        marginHorizontal: 2.5, 
        marginVertical: 8, 
        height: 30, 
        width: 30,
        backgroundColor: '#C4C4C4', 
        justifyContent: 'center', 
        alignItems: 'center', 
        borderRadius: 30/2,
      }
});
