import React from 'react';
import { Scene, Router, Stack, Drawer } from 'react-native-router-flux';
import { Dimensions, Platform } from 'react-native';

import DrawerBar from './component/Drawer/Drawer';
import Splash from './container/Splash/Splash';
import WhiteBoard from './container/WhiteBoard/WhiteBoard';
import VideoCallExample from './container/VideoCallExample/VideoCallExample';

import { connect } from "react-redux";
import { DrawerContent } from './component/Drawer/DrawerContent';

var width = Dimensions.get('window').width;

const RouterWithRedux = connect()(Router);

class Root extends React.Component {

  render() {
    return (
      <RouterWithRedux>
        <Scene key="root" hideTabBar hideNavBar>
          <Stack key="app">
            <Scene hideNavBar panHandlers={null}>
              <Scene
                initial={true}
                component={Splash}
                hideNavBar={true}
                key="Splash"
                title="Splash"
                wrap={false}
              />
              <Scene
                component={WhiteBoard}
                hideNavBar={true}
                wrap={false}
                key="WhiteBoard"
                title="WhiteBoard"
              />
              <Scene
                component={VideoCallExample}
                hideNavBar={true}
                wrap={false}
                key="VideoCallExample"
                title="VideoCallExample"
              />
              {/* <Drawer
                hideNavBar
                key="drawer"
                contentComponent={DrawerBar}
                wrap={false}
                drawerWidth={width - 100}>
                <Scene
                  component={Sites}
                  hideNavBar={true}
                  wrap={false}
                  key="Sites"
                  title="Sites"
                />
              </Drawer> */}
             
            </Scene>
          </Stack>
        </Scene>
      </RouterWithRedux>
    );
  }
}

export default Root;
console.disableYellowBox = true;
